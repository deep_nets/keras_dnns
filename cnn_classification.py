import numpy as np
np.random.seed(7)
from keras import models
from keras.layers import Conv2D, BatchNormalization, Flatten, Dense, MaxPooling2D
from keras.datasets import mnist
from keras.utils.np_utils import to_categorical


def load_mnist():
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()
    train_features = train_images.reshape([60000, 28, 28, 1]).astype(float) / 255
    test_features = test_images.reshape([10000, 28, 28, 1]).astype(float) / 255
    train_labels = to_categorical(train_labels)
    test_labels = to_categorical(test_labels)
    return train_features, test_features, train_labels, test_labels


def cnn_model():
    model = models.Sequential()
    model.add(Conv2D(40, (3, 3), activation='relu', input_shape=(28, 28, 1)))
    model.add(MaxPooling2D((2, 2)))
    model.add(BatchNormalization())
    model.add(Conv2D(80, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2)))
    model.add(BatchNormalization())
    model.add(Conv2D(80, (3, 3), activation='relu'))
    model.add(Flatten())
    model.add(BatchNormalization())
    model.add(Dense(80, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model

def fit_data():
    x_train, x_test, y_train, y_test = load_mnist()
    model = cnn_model()
    hist = model.fit(x_train, y_train, epochs=4, batch_size=64, shuffle=False, verbose=0)
    return model.evaluate(x_test, y_test)

print(fit_data())
