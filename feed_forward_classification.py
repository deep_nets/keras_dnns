import numpy as np
np.random.seed(7)
from keras import layers
from keras import models
from keras.utils.np_utils import to_categorical
from keras.datasets import reuters


def vectorize(seqs, dimension=10000):
    results = np.zeros((len(seqs), dimension),dtype=int)
    for i, seq in enumerate(seqs):
        results[i, seq] = 1
    return results


def load_reuters_data():
    (train_data, tr_labels), (test_data, test_labels) = reuters.load_data(num_words=10000)
    return vectorize(train_data), vectorize(test_data), to_categorical(tr_labels), to_categorical(test_labels)


def construct_model(dropout = 0.5):
    model = models.Sequential()
    model.add(layers.Dense(128,activation='relu',input_shape=(10000,)))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(dropout))
    model.add(layers.Dense(128,activation='relu'))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(dropout))
    model.add(layers.Dense(46,activation='softmax'))
    model.compile(optimizer='adam', loss='categorical_crossentropy',
             metrics=['accuracy'])
    return model


def fit_data():
    x_train, x_test, y_train, y_test = load_reuters_data()
    model = construct_model()
    hist = model.fit(x_train, y_train, epochs=10, batch_size=512, shuffle=False)
    return model.evaluate(x_test, y_test)

print(fit_data())
