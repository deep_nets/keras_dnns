from keras.datasets import imdb
from keras.preprocessing import sequence
from keras.layers import Embedding, Bidirectional, LSTM, Dense
from keras.layers import BatchNormalization
from keras.models import Sequential


MAX_LEN = 500


def load_imdb():
    (x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=10000)
    x_train = sequence.pad_sequences(x_train, maxlen = MAX_LEN)
    x_test = sequence.pad_sequences(x_test, maxlen = MAX_LEN)
    return x_train, x_test, y_train, y_test


def bidirectional_lstm():
    model = Sequential()
    model.add(Embedding(10000, 32))
    model.add(BatchNormalization())
    model.add(Bidirectional(LSTM(32)))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])
    return model


def fit_data():
    x_train, x_test, y_train, y_test = load_imdb()
    model = bidirectional_lstm()
    hist = model.fit(x_train, y_train, epochs=10, batch_size=128, validation_split=0.2)
    return model.evaluate(x_test, y_test)


print(fit_data())
